//
//  AppDelegate.swift
//  Leadsdoit Test App
//
//  Created by Rozhkov Lev on 07.04.2021.
//

import UIKit
import Firebase
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let notificationManager = NotificationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        notificationManager.notificationCenter.delegate = notificationManager
        notificationManager.notificationAuthorizationRequest()
        
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: nil)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
}


