//
//  NotificationManager.swift
//  Leadsdoit Test App
//
//  Created by Rozhkov Lev on 07.04.2021.
//

import Foundation

import UserNotifications

class NotificationManager: NSObject {
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    func notificationAuthorizationRequest() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) { didAllow, error in }
    }
    
    func sendNotification(type: NotificationType) {
        let notificationRequest = NotificationFactory.standard.createNotificationRequest(type: type)
        notificationCenter.add(notificationRequest)
    }
}

//MARK: - UNUserNotificationCenterDelegate

extension NotificationManager: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.banner, .sound])
    }
}
