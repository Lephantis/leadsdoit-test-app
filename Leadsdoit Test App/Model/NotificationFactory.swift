//
//  NotificationFactory.swift
//  Leadsdoit Test App
//
//  Created by Rozhkov Lev on 07.04.2021.
//

import Foundation
import UserNotifications

enum NotificationType {
    case whiteScreenPresented
    case linkOpened
}

class NotificationFactory {
    static let standard = NotificationFactory()
    
    func createNotificationRequest(type: NotificationType) -> UNNotificationRequest{
        let notification = UNMutableNotificationContent()
        var identifier = ""
        switch type {
        case .whiteScreenPresented:
            notification.title = "White screen presented!"
            notification.body = "You have been shown a white screen"
            notification.sound = UNNotificationSound.default
            notification.badge = 1
            identifier = "White Screen Presented Notification"
        case .linkOpened:
            notification.title = "Link opened!"
            notification.sound = UNNotificationSound.default
            notification.badge = 1
            identifier = "Link Opened Notification"
        }
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: notification, trigger: trigger)
        return request
    }
    
}
