//
//  CustomEventType.swift
//  Leadsdoit Test App
//
//  Created by Rozhkov Lev on 07.04.2021.
//

import Foundation

enum CustomEventType: String {
    case whiteScreenPresented = "white_screen_presented"
    case linkOpened = "link_opened"
}
