//
//  ViewController.swift
//  Leadsdoit Test App
//
//  Created by Rozhkov Lev on 07.04.2021.
//

import UIKit
import WebKit
import Firebase

class MainController: UIViewController {
    
    //MARK: - References
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK: - Constants
    
    let link = "https://i.i-bbva.com/click.php?key=q1vm5z6j1lfitwu2wng1&p=partner&c=creative&l=lander&off=offer&link=link"
    let targetLink = "https://www.google.com/"
    
    let whiteScreenIdentifier = "White Screen Controller"
    
    //MARK: - Properties
    
    var isFirstWebViewAction = true
    
    //MARK: - Outlets
    
    @IBOutlet weak var webView: WKWebView!
    
    //MARK: - UIViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpWebView()
    }
    
    //MARK: - Methods
    
    func setUpWebView() {
        webView.navigationDelegate = self
        let request = URLRequest(url: URL(string: link)!)
        webView.load(request)
    }
    
    func hideWebView() {
        webView.stopLoading()
        webView.isHidden = true
    }
    
    func presentWhiteScreen() {
        let whiteScreenController = storyboard?.instantiateViewController(identifier: whiteScreenIdentifier) as! WhiteScreenController
        present(whiteScreenController, animated: false, completion: nil)
    }
    
    func sendLinkOpenedNotification() {
        appDelegate.notificationManager.sendNotification(type: .linkOpened)
    }
    
    func logLinkOpenedEvent() {
        Analytics.logEvent(CustomEventType.linkOpened.rawValue, parameters: nil)
    }
}

//MARK: - WKNavigationDelegate

extension MainController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        
        if isFirstWebViewAction,
           let currentLink = webView.url?.absoluteString {
            
            isFirstWebViewAction = false
            if currentLink == targetLink {
                hideWebView()
                presentWhiteScreen()
            } else {
                logLinkOpenedEvent()
                sendLinkOpenedNotification()
            }
        }
    }
}

