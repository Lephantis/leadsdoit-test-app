//
//  WhiteScreenController.swift
//  Leadsdoit Test App
//
//  Created by Rozhkov Lev on 07.04.2021.
//

import UIKit
import Firebase

class WhiteScreenController: UIViewController {
    
    //MARK: - References
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK: - UIViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logWhiteScreenPresentedEvent()
        sendWhiteScreenPresentedNotification()
    }
    
    //MARK: - Methods
    
    func logWhiteScreenPresentedEvent() {
        Analytics.logEvent(CustomEventType.whiteScreenPresented.rawValue, parameters: nil)
    }
    
    func sendWhiteScreenPresentedNotification() {
        appDelegate.notificationManager.sendNotification(type: .whiteScreenPresented)
    }
}
